//
//  ContentView.swift
//  gittest
//
//  Created by AirLogFUN on 2020/12/18.
//  Copyright © 2020 AirLogFUN. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
